#include "Piece.h"


Piece::Piece() :
	Piece(Color::None)
{
	//empty
}

Piece::Piece(const Color & color) :
	m_color(color)
{
	//empty
	static_assert(sizeof(*this) <= 2, "This class should be 2 byte in size");
}

Piece::Piece(const Piece & other)
{
	*this = other;
}

Piece::Piece(Piece && other)
{
	*this = std::move(other);
}

Piece::~Piece()
{
	m_color = Color::None;
}

Piece::Color Piece::getColor() const
{
	return m_color;
}



void Piece::setColor(const Color& color)
{
	this->m_color = color;
}

Piece & Piece::operator=(const Piece & other)
{
	m_color = other.m_color;

	return *this;
}

Piece & Piece::operator=(Piece && other)
{
	m_color = other.m_color;

	new(&other) Piece;
	return *this;
}

std::ostream & operator<<(std::ostream & os, const Piece & piece)
{
	switch (static_cast<uint8_t>(piece.m_color))
	{
	case 0:
		std::cout << "(   )";
		return os;
		break;
	case 1:
		std::cout << "( B )";
		return os;
		break;
	case 2:
		std::cout << "( W )";
		return os;
		break;
	default:
		return os;
	}
}

