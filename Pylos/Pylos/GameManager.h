#pragma once
#include "../Logging/Logging.h"
#include "Board.h"
#include "Piece.h"
#include <fstream>
#include <string>
#include <regex>
#include <memory>
class GameManager
{
public:
	struct Player
	{
		// Player1 has 15 Pieces
		int countPieces = 15;
		// Player1 has White Pieces
		Piece::Color color = Piece::Color::White;
		// Player1 has number 1
		int number = 1;
	};

public:
	GameManager();
	~GameManager();
public:
	// Stats the game
	void playGame();
	// Switches a pointer from player1 to player2 and vice-versa
	void switchPlayer();
	// Gets the playerInput
	std::string getPlayerInput() const;
	// Gets new index from new coordinates
	void refreshIndexFromNewCoordinates(int &pieceNewIndex, const std::string& errorMessage);
	// The playerInput is stored in playerLastCommand
	std::string playerLastCommand;
	// Does something based on playerLastCommand
	void doPlayerCommand(std::string &playerLastCommand);
	// Gets the index from the coordinates pushed in by the players
	int getIndexFromCoordinates(std::string &input_coordinates);
	// Moves a piece from the board to on top of a square
	void movePieceFromBoardToUpperLevel(int pieceOldPos, int pieceNewPos);
	// Takes 1 to 2 pieces if a square is made out of same colors
	void takePiecesFromBoard(int pieceNewPos);
	// Puts a piece on the board from Reserve
	void placePieceFromReserve(int pieceNewPos) ;
	// Fixes the availability of 4 pieces
	void fixSquareBelow(int upperPieceIndex, bool newAvailability) ;
	// Shows the winner
	void showWinner() const;
	// Stores the coordinates
	std::string coordinates;
	// Checks if a piece is movable
	bool checkMoveablePiece(int pieceIndex) const;
	// Piece old pos index, Piece new pos index
	int pieceOldPos;
	int pieceNewPos;
public:
	// Basically the game with the pieces
	Board game;
	// The two players
	Player player1;
	Player player2;
	// A pointer to the current player, player1 starts first
	Player *currentPlayer = &player1;
	mutable bool foundWinner = 0;
};
