#include "stdafx.h"
#include "CppUnitTest.h"
#include "../Pylos/Piece.h"


using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace Testing
{
	TEST_CLASS(PieceTests)
	{
	public:

		TEST_METHOD(WhitePieceTest)
		{
			Piece piece(Piece::Color::White);
			Assert::IsTrue(piece.getColor() == Piece::Color::White);
		}

		TEST_METHOD(BlackPieceTest)
		{
			Piece piece(Piece::Color::Black);
			Assert::IsTrue(piece.getColor() == Piece::Color::Black);
		}

		TEST_METHOD(NonePieceTest)
		{
			Piece piece(Piece::Color::None);
			Assert::IsTrue(piece.getColor() == Piece::Color::None);
		}

	};
}