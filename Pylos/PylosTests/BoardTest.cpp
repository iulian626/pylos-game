#include "stdafx.h"
#include "CppUnitTest.h"
#include "../Pylos/Board.h"


using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace Testing
{
	TEST_CLASS(BoardTests)
	{
	public:
		TEST_METHOD(FirstLayerIsAvailable)
		{
			Board game;
			Assert::IsTrue(game.board[0].first.availability == 1);
		}


		TEST_METHOD(OtherLayersAreNotAvailable)
		{
			Board game;
			Assert::IsTrue(game.board[16].first.availability == 0 && game.board[25].first.availability == 0 && game.board[29].first.availability == 0);
		}

		TEST_METHOD(checkSquareTest)
		{
			Board game;
			game.board[0].second.setColor(Piece::Color::White);
			game.board[1].second.setColor(Piece::Color::White);
			game.board[4].second.setColor(Piece::Color::White);
			game.board[5].second.setColor(Piece::Color::White);
			game.checkSquare();
			Assert::IsTrue(game.board[16].first.availability == 1);
		}

		TEST_METHOD(checkMovablePiece)
		{
			Board game;
			Assert::IsFalse(game.checkForTakeablePieces(Piece::Color::Black));
		}



	};
}